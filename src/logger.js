// TODO: сделать отдельный метод для конфигурирования

const colors = require('colors');
const fs = require('fs');
const path = require('path');
const JSONCircular = require('/obereg/util/json_circular.js');

const LENGTH_NAME = 7;

/**
 * @typedef {("black"|"red"|"green"|"yellow"|"blue"|"magenta"|"cyan"|"white"|"gray"|"grey"|
 * "bgBlack"|"bgRed"|"bgGreen"|"bgYellow"|"bgBlue"|"bgMagenta"|"bgCyan"|"bgWhite"|
 * "blackBG"|"redBG"|"greenBG"|"yellowBG"|"blueBG"|"magentaBG"|"cyanBG"|"whiteBG"|
 * "reset"|"bold"|"dim"|"italic"|"underline"|"inverse"|"hidden"|"strikethrough"|
 * "rainbow"|"zebra"|"america"|"trap"|"random")} Color
 */

class Log {
    /**
     * @param {object}                   option
     * @param {string}                   option.name
     * @param {string | object}          [option.level]
     * @param {Color | array<Color>}     [option.color]
     */
    constructor(option = {}) {
        this.loggers = [];
        this._showTime = option.time || false;
        this.level = option.level || Log.LEVELS.ALL;
        // console.log('Log::constructor - _showTime', this._showTime);
        this.name = option.name || '';


        this.nameF = (this.name.length % LENGTH_NAME === 0)
            ? (this.name)
            : (this.name + ' '.repeat(LENGTH_NAME - (this.name.length % LENGTH_NAME)));


        if (Array.isArray(option.color)) {
            this._color = option.color;
        } else if (option.color) {
            this._color = [option.color];
        } else {
            this._color = [];
        }
        this.nameC = this._addColor(this.nameF, this._color);

    }

    _addColor(str, colors) {
        while (colors.length) {
            str = str[colors.pop()];
        }
        return str;
    }

    _log(level, title, option = {}, args) {
        option.date = new Date();
        for (let i in this.loggers) {
            this.loggers[i](level, title, option, args);
        }
    }

    /**
     *
     * @param                   [logger]
     * @param {object}          [setting]
     * @param {string}          [setting.file]
     * @param {string | object} [setting.level]
     * @param {number}          [setting.limitInFile]
     * @param {number}          [setting.limitFiles]
     * @returns {Log}
     */
    addLogger(logger = Log.console, setting = {}) {
        if (!setting.level) {
            setting.level = this.level.id;
        } else if (typeof setting.level === 'object') {
            setting.level = setting.level.id;
        }
        this.loggers.push(logger(this, setting));
        return this;
    }

    setShowTime(flag) {
        // console.log('Log::setShowTime - _showTime', flag);
        this._showTime = flag;
        return this;
    }


    /** //////////////////////////////////----| loggers |----//////////////////////////////////////////////////////////*/
    static console(context, setting = {}) {
        return function (level, title = '', option, args) {
            if (setting.level < level.id) {
                return this;
            }
            let time = '';
            // console.log('Log::console - _showTime', this._showTime);
            if (this._showTime) {
                time = option.date.toLocaleTimeString() + '.' + option.date.getMilliseconds() + '\t';
            }
            /* for (const i in args) {
                 if (typeof args[i] === 'object') {
                     args[i] = JSONCircular(args[i], null, '  ');
                 }
             } */
            console[option.method](level.nameC + ' ' + this.nameC + time + '||', title, ...args);
            return this;
        }.bind(context);
    }

    static file(context, setting/*, limitInFile = 10000, limitFiles = 20*/) {
        if (!setting.limitInFile) {
            setting.limitInFile = 10000;
        }
        if (!setting.limitFiles) {
            setting.limitFiles = 30;
        }

        let currentFile, logFile
            , countEntry = 0
        ;


        const dir = path.dirname(setting.file);
        if (!fs.existsSync(dir)) {
            fs.mkdir(dir, function (err) {
                if (err) {
                    console.error(err);
                }
            });
        }
        initWrite();

        return function (level, title = '', option, args) {
            if (setting.level < level.id) {
                return this;
            }
            const _args = args.slice();
            for (const i in _args) {
                if (typeof _args[i] === 'object') {
                    _args[i] = JSON.stringify(_args[i]);
                }
            }
            let msg = level.name + ':\t'
                + this.nameF + ':\t'
                + option.date.toLocaleString() + '.' + option.date.getMilliseconds()
                + '\t|| ' + title + ' ' + _args.join(' ');


            msg += '\n';
            // console.log('write in file', msg);
            logFile.write(new Buffer(msg), onWriteDone);
            // logFile.write(new Buffer(JSON.stringify({Level: level.id, Name: this.name, Date: option.date, Title: title, Data: args})), function (err) {
            //     if (err) {
            //         console.error(err);
            //     }
            // });
            return this;
        }.bind(context);

        function onWriteDone(err) {
            if (err) {
                console.error(err);
            } else {
                countEntry++;
                if (countEntry > setting.limitInFile) {
                    initWrite();
                    countEntry = 0;
                }
            }
        }

        function initWrite() {
            // TODO: init write only then something log
            if (logFile) {
                logFile.close();
            }

            clearOldFile(setting.file);

            const date = new Date();
            currentFile = setting.file + '_' + date.toLocaleString() + '.' + date.getMilliseconds();
            fs.accessSync(path.dirname(setting.file), fs.constants.W_OK);
            logFile = fs.createWriteStream(currentFile, {flags: 'a'});
        }

        function clearOldFile(filenamePattern) {
            const dir = path.dirname(filenamePattern);
            const name = path.basename(filenamePattern);
            fs.readdir(dir, function (err, files) {
                if (err) {
                    console.error('Clear dir with logs', err);
                    return;
                }
                files = files.filter(file => file.includes(name));


                while (files.length > setting.limitFiles) {
                    const file = path.join(dir, files.shift());
                    fs.unlink(file, function (err) {
                        if (err) {
                            console.error('Delete old file', err);
                        }
                    })
                }
            })
        }
    }

    static server() {
        const connect = new (require('./connect'))({
            host:                 '192.168.0.51'
            // host: 'localhost'
            , port:               8000
            , rejectUnauthorized: false
            , User:               context.User
        });
        return function (level, title, option, args) {
            console.log('connect');
            connect.send({Level: level.id, Name: this.name, Date: option.date, Title: title, Data: args});
        }.bind(context);


        // logss.push({Level: level.id, Name: this.name, Date: option.date, Title: title, Data: args})
    }


    /**\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\----\ loggers |---\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\*/


    debug(title, ...args) {
        this._log(Log.LEVELS.DEBUG, title, {method: 'log'}, args);
    }

    info(title, ...args) {
        this._log(Log.LEVELS.INFO, title, {method: 'log'}, args);
    }

    notice(title, ...args) {
        this._log(Log.LEVELS.NOTICE, title, {method: 'log'}, args);
    }

    note(title, ...args) {
        this._log(Log.LEVELS.NOTICE, title, {method: 'log'}, args);
    }

    warn(title, ...args) {
        this._log(Log.LEVELS.WARM, title, {method: 'warn'}, args);
    }

    error(title, ...args) {
        this._log(Log.LEVELS.ERROR, title, {method: 'error'}, args);
    }

    fatal(title, ...args) {
        this._log(Log.LEVELS.FATAL, title, {method: 'error'}, args);
    }

    assert(condition, title, ...args) {
        // TODO: add result output method
        let level = Log.LEVELS.FAIL;
        const option = {method: 'error'};
        if (condition) {
            level = Log.LEVELS.SUCCESS;
            option.method = 'log';
        }
        this._log(level, title, option, args);
    }
}

if (!Error.prototype.toJSON) {
    Error.prototype.toJSON = function () {
        const obj = {message: this.message, stack: this.stack};
        const keys = Object.keys(this);
        for (const key of keys) {
            obj[key] = this[key];
        }
        return JSON.stringify(obj);
    };
} else {
    console.warn('Error has in .prototype toJSON');
}


Log.LEVELS = {
    ALL:       {
        name: 'ALL'
        , id: 999
    }
    , SUCCESS: {
        name:    'OK'
        , nameC: '[  OK  ]'.green
        , id:    8
    }
    , DEBUG:   {
        name:    'DEBUG'
        , nameC: '[DEBUG ]'.gray
        , id:    7
    }
    , INFO:    {
        name:    'INFO'
        , nameC: '[ INFO ]'.cyan
        , id:    6
    }
    , NOTICE:  {
        name:    'NOTICE'
        , nameC: '[ NOTE ]'.magenta
        , id:    5
    }
    , NOTE:    {
        name:    'NOTICE'
        , nameC: '[ NOTE ]'.magenta
        , id:    5
    }
    , WARM:    {
        name:    'WARM'
        , nameC: '[ WARM ]'.yellow.dim
        , id:    4
    }
    , ERROR:   {
        name:    'ERROR'
        , nameC: '[ERROR ]'.red.bgWhite
        , id:    3
    }
    , FAIL:    {
        name:    'FAIL'
        , nameC: '[ FAIL ]'.red.bgYellow
        , id:    2
    }
    , FATAL:   {
        name:    'FATAL'
        , nameC: '[FATAL ]'.white.bgRed
        , id:    1
    }
    , NONE:    {
        name: 'NONE'
        , id: 0
    }
};
Log.colors = colors;


module.exports = Log;