const tls = require('tls');
const Log = require('./logger');
const fs = require('fs');


const log = new Log({name: 'TLSConnect'});


// process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";


class TLSConnect {
    constructor(option) {
        this._o = option;
        this.state = 0;
        this._notSend = [];
        this._handlers = [];
        this._addHandlers();

        this._startConnection();
    }

    _startConnection() {
        log.notice('startConnection');
        this._socket = tls.connect(this._o);
        this._bind();
    }


    on(event, func) {
        this._on(event, func);
        this._socket.on(event, func);
        return this;
    }

    _on(event, func) {
        this._handlers.push({event: event, func: func});
        return this;
    }

    send(data) {
        if (this.state) {
            // data = JSON.stringify(data);
            log.debug('send', data.length);
            this._socket.write(data);
        } else {
            this._notSend.push(data);
        }
        return this;
    }

    close() {
        this._closed = true;
        this._socket.destroy();
    }

    _addHandlers() {
        this._on('error', this._onError.bind(this))
            ._on('close', this._onClose.bind(this))
            ._on('connect', this._onConnect.bind(this))
        // ._on('data', this._onData.bind(this))
    }

    _bind() {
        for (const handler of this._handlers) {
            this._socket.on(handler.event, handler.func);
        }
    }


    _onConnect(...args) {
        const address = this._socket.address();
        log.notice('Connect to', address.address + ':' + address.port);
        this.state = 1;
        this.send({User: this._o.User});
        this._sendWaitingData();
    }


    _sendWaitingData() {
        while (this._notSend.length > 0) {
            this.send(this._notSend.shift());
        }
    }

    _onData(args) {
        args = args.toString();
        log.debug('Receive', args);
    }

    _onError(err) {
        this.state = 0;
        log.error('ERROR', err);
    }

    _onClose() {
        this.state = 0;
        log.warn('CLOSE');
        if (!this._closed) {
            setTimeout(this._startConnection.bind(this), 5000);
        }
    }
}


// // TEST LINE(S) ////////////////////////
// const option = {
//     host: '192.168.1.95'
//     // host: 'localhost'
//     , key: fs.readFileSync('./certs/client.key')
//     , cert: fs.readFileSync('./certs/client.crt')
//     , ca: [fs.readFileSync('./certs/ca.crt')]
//     , port: 4545
// //         , port: 8000
//     , rejectUnauthorized: false
//     , User: 'TEST'
// };
// const conn = new TLSConnect(option)
//     // .send({Level: 1, Name: 'TEST Name', Date: 'TEST Date', Title: 'TEST Title', Data: 'TEST Data'})
//         .on('data', function (data) {
//             log.debug('Receive', data.toString());
//         })
// ;
// let count = 0;
// setInterval(function () {
//     conn.send({count: ++count, time: new Date().toLocaleTimeString()});
// }, 5000);

module.exports = TLSConnect;