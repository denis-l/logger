console.log('this logger test');

const Log = require('./logger');
const should = require('should');
// import Log from '/home/we/Рабочий стол/js/log-server/.build/logger/logger.js'
// import should from 'should'

describe('Logger', function () {
    let log;
    before(function () {
        log = new Log({name: 'TEST'})
            .addLogger(Log.console)
            .setShowTime(true)
        ;
        try {
            log.addLogger(Log.file, {file: './.log', level: Log.LEVELS.ALL})
        } catch (e) {
            console.log('Error before', e)
        }
    });
    let logAll = function () {
        log.debug('debug');
        log.info('info');
        log.notice('notice');
        log.note('note');
        log.warn('warn');
        log.error('error', new Error('test error'));
        log.fatal('fatal');
        log.assert(true, 'assert true');
        log.assert(false, 'assert false');
    };

    describe('Call log', function () {
        context('', function () {
            it('Level ALL', function () {
                (function () {
                    logAll();
                }).should.not.throw();
            });
            it('Level ERROR', function () {
                (function () {
                    logAll();
                }).should.not.throw();
            });
            it('Without time', function () {
                (function () {
                    log.setShowTime(false);
                    logAll();
                }).should.not.throw();
            });
            // it('should return -1', function () {
            //     [1, 2, 3].indexOf(4).should.equal(-1);
            // });
        });
        // context('when present', function () {
        //     it('should return the index where the element first appears in the array', function () {
        //         [1, 2, 3].indexOf(3).should.equal(2);
        //     });
        // });
    });
});
